# Vector Kirigami Parser

Vers 0.1
License : GPL3

Licence of course, excludes the papers included for reference. If any problems they will be removed immediately.

This project is an example on how you can create .svg graphical format manually. This can be helpful for anyone trying for example to 3D print complex structures, or really understand how to make algorithmic 2D designs.

As demonstration, and a little hackely, we draw a simple line pattern in pattern1 and a little more advanced bistable parametric squared pattern. Deformations can be done in a bistable-positive Poisson coefficient in all directions. 
