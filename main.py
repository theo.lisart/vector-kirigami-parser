"""
FabZero - Parametric bistable kirigami svg parser

Author : Théo Lisart
2020-2021
License : GPL3
"""
import math as m

#Meta-data
output_file_name = "long_squares.svg"
author = "Théo Lisart"

# File Description
image_width = 400
image_length = 200
cut_stroke = 0.1      # Drawing stroke
cell_size = (20, 20)
cut_space = 1.2         # Space between lines in basic pattern
amount_basics_cut = 4 # Amount of cuts of the most basic cell


# Drawing
line_growth = 0.70
color = [0, 0, 0]    # Black color
angle = 360/(amount_basics_cut)
bar_length = line_growth*cell_size[0]
# ---------------------- Geometry definitions ----------------------------------

def line(pos_x1, pos_y1, pos_x2, pos_y2, stroke, rgb) :

    """
    Returns the string encoding of the XML line
    Color parameter is included, as in the 3D printer color can define various
    laser parameters

    pos_x1 = floating point - initial point
    pos_y1 = floating point - initial point
    pos_x2 = floating point - arrival point
    pos_y2 = floating point - arrival point

    stroke = floating point (should be generally set)
    rgb = [R, G, B], int[3] array
    """

    line_string = "<line x1=\""+ str(pos_x1) + "\" y1=\""+ str(pos_y1) + "\" x2=\"" + str(pos_x2) + "\" y2=\"" + str(pos_y2) + "\" style=\"stroke:rgb(" + str(rgb[0]) +","+str(rgb[1]) + ","+str(rgb[2]) + ")" + ";stroke-width:" + str(stroke) + "\" />"
    return line_string

def rotate(line, angle) :
    """
    Rotate a line as vector in the 2D space, attention to the face that the y axes
    points downward.

    simple matvec operation

     _                    _
    |cos(theta) -sin(theta)|
    |sin(theta)  cos(theta)|
    -                      -

    All input vectors are supposed to be at the origin. A generalization is easy
    to implement but requires extra translations.
    """

    vec = (line[2], line[3])
    return (0, 0, vec[0]*m.cos(angle) + vec[1]*m.sin(angle), -vec[0]*m.sin(angle)+ vec[1]*m.cos(angle))


def line_translate(x_offset, y_offset, line) :
    return (line[0] + x_offset, line[1] + y_offset, line[2] + x_offset, line[3] + y_offset)

def offset_pattern(x_offset, y_offset, all_lines) :
    """
    Returns from a list of quadruples a list of quadruples translated to the asked position
    """

    moved_pattern = []
    for i in range(len(all_lines)):
        new_line = line_translate(x_offset, y_offset, all_lines[i])
        moved_pattern.append(new_line)

    return moved_pattern

def flip_x(all_lines) :
    """
    Returns a list of lines of the flipped along default x axis
    Apply bitflip : U = [[1, 0], [0, -1]]
    """

    new_line_list = []

    for i in range(len(all_lines)) :
        buffer_line = (all_lines[i][0], -all_lines[i][1], all_lines[i][2], -all_lines[i][3])
        new_line_list.append(buffer_line)
    return new_line_list


def flip_y(all_lines) :
    """
    Returns a list of lines of the flipped along default y axis
        Apply bitflip : U = [[-1, 0], [0, 1]]
    """

    new_line_list = []

    for i in range(len(all_lines)) :
        buffer_line = (-all_lines[i][0], all_lines[i][1], -all_lines[i][2], all_lines[i][3])
        new_line_list.append(buffer_line)
    return new_line_list


def pattern_1(total_width, total_length, stroke, csize, cut_space) :
    """
    Simple line pattern, vertical deformation, alternating lines

    -
    total_width
    total_length
    stroke
    csize
    cut_space
    """

    # _Init_ output string
    geometry_string = ""

    # _Init_ positionning - basic cell definition then reproduction to fill entire sheet
    #init_point = (0, 0)
    x_offset = csize[0]
    y_offset = csize[1]

    cells_x = round(total_width/x_offset)
    cells_y = round(total_length/y_offset)

    # Creating the basic cell
    cell_geometry = []    # List of quadruples indicades (x1, y1, x2, y2), listing all lines in first cell

    for i in range(cells_x) :
        for j in range(cells_y) :

            # Horizontal lines
            if(j%2 == 0):
                line_center_horiz= (i*x_offset, y_offset + j*y_offset, x_offset - cut_space/2 + i*x_offset, y_offset + j*y_offset)
                cell_geometry.append(line_center_horiz)

            elif(j%2==1) :
                line_center_horiz= (i*x_offset + x_offset/2, y_offset + j*y_offset, x_offset + x_offset/2 - cut_space/2 + i*x_offset, y_offset + j*y_offset)
                cell_geometry.append(line_center_horiz)

    # Drawing
    color = [0, 0, 0]
    for i in range(len(cell_geometry)) :
        geometry_string += line(cell_geometry[i][0], cell_geometry[i][1], cell_geometry[i][2], cell_geometry[i][3], stroke, color) + "\n"

    return geometry_string


def pattern_2(total_width, total_length, stroke, csize, cut_space, angle, bar_length, amount_basics_cut) :

    """
    total_width = float paper width
    total_length = float paper length
    stroke = draw stroke, should be set as the laser cross section
    csize = tuple (x length cell, y length cell)
    cut_space = float space between cuts
    angle = opening of the design
    bar_length = float, length of the basic cut length

    For this design the unit cell must be a square (X, X)
    Here we exploit various symmetries shown in the pattern
    """

    # _Init_ output string
    geometry_string = ""

    # _Init_ positionning - basic cell definition then reproduction to fill entire sheet
    #init_point = (0, 0)
    x_offset = csize[0]
    y_offset = csize[1]

    # Design parameters
    t = cut_space
    theta = angle
    a = bar_length
    n = amount_basics_cut

    # Creating the basic cell
    cell_geometry = []    # List of quadruples indicades (x1, y1, x2, y2), listing all lines in first cell

    # Creating the entire sheet
    cells_x = round(total_width/(2*x_offset))
    cells_y = round(total_length/(2*y_offset))

    # Unit structure
    """
        Strategy : creating four identical lines, rotating each lines and placing them where they sould be
        on the basic building block structure
    """

    for i in range(n) :
        buffer_line = (0,0, a*m.sin(theta), a*m.cos(theta))

        # Rotating around the origin
        buffer_line = rotate(buffer_line, i*m.pi/2)    ## DEBUG, needs to be generalized

        # Translating
        if(i == 0) :
            buffer_line = line_translate(0, y_offset -t, buffer_line)
        if(i == 1) :
            buffer_line = line_translate(x_offset - t, y_offset, buffer_line)
        if(i == 2) :
            buffer_line = line_translate(x_offset, t, buffer_line)
        if(i == 3) :
            buffer_line = line_translate(t, 0, buffer_line)

        # Recording
        cell_geometry.append(buffer_line)


    # Unit cell

    """
    We drew mathematically all four lines, the basic cell is a unfolded mirror-symmetry
    of this pattern in all NSEO directions. The final pattern is then simply duplicated.


    To save memory, the previous structures are not recorded into a dynamic structure,
    but drawn step by step
    """
    new_lines_x = flip_x(cell_geometry)
    for i in range(len(new_lines_x)) :
        cell_geometry.append(new_lines_x[i])

    new_lines_y = flip_y(cell_geometry)
    for i in range(len(new_lines_y)) :
        cell_geometry.append(new_lines_y[i])


    # Placing the basic cell at the top left corner

    cell_geometry = offset_pattern(x_offset, y_offset, cell_geometry)
    # Replicating along the sheet

    for i in range(cells_y) :
        for j in range(cells_x) :

            current_geometry = offset_pattern(2*j*x_offset, 2*i*y_offset, cell_geometry)

            # Drawing
            color = [0, 0, 0]
            for k in range(len(current_geometry)) :
                geometry_string += line(current_geometry[k][0], current_geometry[k][1], current_geometry[k][2], current_geometry[k][3], stroke, color) + "\n"

    return geometry_string

# CVG metadata -----------------------------------------------------------------
init_string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
geometry_string = "<svg width=\"" + str(image_width) + "mm" + "\" height=\"" + str(image_length) + "mm" + "\" version=\"1.1\" viewBox=\"0 0 " + str(image_width) + " " + str(image_length) + "\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:cc=\"http://creativecommons.org/ns#\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"

# ---------------------- Creating file -----------------------------------------

# init file
file = open("output/" + output_file_name, "x")    #DEBUG should put a test here if the file already exists (try-catch)
file.write(init_string + "\n")

# defining the geometry of the file
file.write(geometry_string)
# ------------------------------------------------------------------------------

#file.write(pattern_1(image_width, image_length, cut_stroke, cell_size, cut_space) )
file.write(pattern_2(image_width, image_length, cut_stroke, cell_size, cut_space, angle, bar_length, amount_basics_cut) )
